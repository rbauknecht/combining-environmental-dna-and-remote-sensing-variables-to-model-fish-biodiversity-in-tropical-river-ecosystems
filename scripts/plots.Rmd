---
title: "plots"
author: "Robin Bauknecht"
date: "`r Sys.Date()`"
output: html_document
---

## This script contains the code to make additional plots

------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Loading packages and data

```{r, message=FALSE, warning=FALSE}
# Specify a vector of package names
packages_to_check <- c("randomForest","ggplot2","caret","tidyr", "tidyverse", "viridisLite")

# Loop through each package
for (package_name in packages_to_check) {
  # Check if the package is installed
  if (!requireNamespace(package_name, quietly = TRUE)) {
    # If not installed, install it
    install.packages(package_name)
    
    # Load the package
    library(package_name, character.only = TRUE)
  } else {
    # If already installed, just load it
    library(package_name, character.only = TRUE)
  }
}

# Also loading needed functions
source("../scripts/helper_functions.R")

# Loading the data and separating the rivers
load("../R_data/combined_data.Rdata")
data_maroni <- subset(combined_data, river == "maroni")
data_oyapock <- subset(combined_data, river == "oyapock")
data_kinabatangan <- subset(combined_data, river == "kinabatangan")
data_magdalena <- subset(combined_data, river == "magdalena")
data_okavango <- subset(combined_data, river == "okavango")
data_casamance <- subset(combined_data, river == "casamance")
combined_data$river <- str_to_title(tolower(combined_data$river))
```

------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Box plot of Richness and Shannon index per river. 

```{r}
# Defining colours of interest
# river_colors <- c("#deebf7","#bdd7ee","#9ecae1","#6baed6","#3182bd", "#08519c")
river_colors <- c("#31688EFF", "#31688EFF", "#31688EFF", "#31688EFF",  "#31688EFF", "#31688EFF")

# Factoring rivers with specified order
combined_data$river <- factor(combined_data$river, levels = c("Kinabatangan", "Magdalena", "Casamance", "Okavango", "Maroni", "Oyapock"))

# Now create richness plot with the modified order
ggplot(combined_data, aes(x = river, y = Richness, fill = river)) +
  geom_boxplot(width = 0.6, alpha=0.7) +
  scale_fill_manual(values = river_colors) +  
  labs(title = "", x = NULL, y = "Species Richness per sample") +
  guides(fill = "none")+
  theme_minimal() +
  theme(axis.text.x = element_text(angle = 35, hjust = 1, size=13),
        plot.margin = unit(c(0.3, 0.3, 0.3, 0.3), "cm"),
        axis.title.x = element_text(margin = margin(t = 13), size=16),
        axis.title.y = element_text(margin = margin(r = 13), size=16),
        plot.background = element_rect(color = "grey", fill = NA, size = 1))

# Now creating shannon plot with the modified order
ggplot(combined_data, aes(x = river, y = Shannon, fill = river)) +
  geom_boxplot(width = 0.6, alpha=0.7) +
  scale_fill_manual(values = river_colors) + 
  labs(title = "",x =NULL, y = "Shannon Index per sample")+
  guides(fill = "none")+  
  theme_minimal() +
  theme(axis.text.x = element_text(angle = 35, hjust = 1, size=13),
        plot.margin = unit(c(0.3, 0.3, 0.3, 0.3), "cm"),
        axis.title.x = element_text(margin = margin(t = 13), size=16),
        axis.title.y = element_text(margin = margin(r = 13), size=16),
        plot.background = element_rect(color = "grey", fill = NA, size = 1))
```

------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Tabulated values

```{r}
summary_table <- combined_data %>%
  group_by(river) %>%
  summarise(
    Median_Richness = median(Richness, na.rm = TRUE),
    Median_Shannon = median(Shannon, na.rm = TRUE)
  )
summary_table
```

------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Scatter plot for global geographic seperated predictions

```{r}
# Loading richness predicted vs observed frames created in the "global_model" script
load("../R_data/pred_obs_casamance_richness.Rdata")
pred_obs_casamance_richness <- pred_obs_frame_richness
pred_obs_casamance_richness$River <- "Casamance"
load("../R_data/pred_obs_maroni_richness.Rdata")
pred_obs_maroni_richness <- pred_obs_frame_richness
pred_obs_maroni_richness$River <- "Maroni"
load("../R_data/pred_obs_oyapock_richness.Rdata")
pred_obs_oyapock_richness <- pred_obs_frame_richness
pred_obs_oyapock_richness$River <- "Oyapock"
load("../R_data/pred_obs_kinabatangan_richness.Rdata")
pred_obs_kinabatangan_richness <- pred_obs_frame_richness
pred_obs_kinabatangan_richness$River <- "Kinabatangan"
load("../R_data/pred_obs_okavango_richness.Rdata")
pred_obs_okavango_richness <- pred_obs_frame_richness
pred_obs_okavango_richness$River <- "Okavango"
load("../R_data/pred_obs_magdalena_richness.Rdata")
pred_obs_magdalena_richness <- pred_obs_frame_richness
pred_obs_magdalena_richness$River <- "Magdalena"

# combining them
combined_pred_obs_richness <- rbind(pred_obs_casamance_richness, pred_obs_maroni_richness)
combined_pred_obs_richness <- rbind(combined_pred_obs_richness, pred_obs_oyapock_richness)
combined_pred_obs_richness <- rbind(combined_pred_obs_richness, pred_obs_kinabatangan_richness)
combined_pred_obs_richness <- rbind(combined_pred_obs_richness, pred_obs_okavango_richness)
combined_pred_obs_richness <- rbind(combined_pred_obs_richness, pred_obs_magdalena_richness)

# Loading Shannon predicted vs observed frames created in the "global_model" script
load("../R_data/pred_obs_casamance_shannon.Rdata")
pred_obs_casamance_shannon <- pred_obs_frame_shannon
pred_obs_casamance_shannon$River <- "Casamance"
load("../R_data/pred_obs_maroni_shannon.Rdata")
pred_obs_maroni_shannon <- pred_obs_frame_shannon
pred_obs_maroni_shannon$River <- "Maroni"
load("../R_data/pred_obs_oyapock_shannon.Rdata")
pred_obs_oyapock_shannon <- pred_obs_frame_shannon
pred_obs_oyapock_shannon$River <- "Oyapock"
load("../R_data/pred_obs_kinabatangan_shannon.Rdata")
pred_obs_kinabatangan_shannon <- pred_obs_frame_shannon
pred_obs_kinabatangan_shannon$River <- "Kinabatangan"
load("../R_data/pred_obs_okavango_shannon.Rdata")
pred_obs_okavango_shannon <- pred_obs_frame_shannon
pred_obs_okavango_shannon$River <- "Okavango"
load("../R_data/pred_obs_magdalena_shannon.Rdata")
pred_obs_magdalena_shannon <- pred_obs_frame_shannon
pred_obs_magdalena_shannon$River <- "Magdalena"

# combining them
combined_pred_obs_shannon <- rbind(pred_obs_casamance_shannon, pred_obs_maroni_shannon)
combined_pred_obs_shannon <- rbind(combined_pred_obs_shannon, pred_obs_oyapock_shannon)
combined_pred_obs_shannon <- rbind(combined_pred_obs_shannon, pred_obs_kinabatangan_shannon)
combined_pred_obs_shannon <- rbind(combined_pred_obs_shannon, pred_obs_okavango_shannon)
combined_pred_obs_shannon <- rbind(combined_pred_obs_shannon, pred_obs_magdalena_shannon)

# defining limits
limits <- c(0, 151)
# defining colours
color_vector <- viridis(6)

# Richness plot
p1 <- ggplot(data = combined_pred_obs_richness, aes(x = pred, y = obs, color = River)) +
  geom_point() +
  labs(x = "Predicted Richness", y = "Observed Richness", color = "River predicted on") +
  ggtitle("") +
  lims(x = limits, y = limits) +
  guides(fill = "none") +
  geom_abline(intercept = 0, slope = 1, linetype = "dashed", color = "blue")+
  scale_color_manual(values = color_vector) +  # Set the color vector
  theme_minimal() +
  theme(plot.margin = unit(c(0.3, 0.3, 0.3, 0.3), "cm"),
        axis.text = element_text(size = 11),
        axis.title.x = element_text(margin = margin(t = 13), size = 14),
        axis.title.y = element_text(margin = margin(r = 13), size = 14),
                legend.title = element_text(size = 12),
        legend.text = element_text(size = 12),
        legend.box.background = element_rect(colour = "grey"),
        plot.background = element_rect(color = "grey", fill = NA, size = 1))


# Shannon limis
limits <- c(0, 5)

# Shannon plot
p2 <- ggplot(data = combined_pred_obs_shannon, aes(x = pred, y = obs, color = River)) +
  geom_point() +
  labs(x = "Predicted Shannon index", y = "Observed Shannon index", color = "River predicted on") +
  ggtitle("") +
  lims(x = limits, y = limits) +
  guides(fill = "none") +
  geom_abline(intercept = 0, slope = 1, linetype = "dashed", color = "blue")+
    scale_color_manual(values = color_vector) +  # Set the color vector
  theme_minimal() +
  theme(plot.margin = unit(c(0.3, 0.3, 0.3, 0.3), "cm"),
        axis.text = element_text(size = 11),
        axis.title.x = element_text(margin = margin(t = 13), size = 14),
        axis.title.y = element_text(margin = margin(r = 13), size = 14),
        legend.title = element_text(size = 12),
        legend.text = element_text(size = 12),
        legend.box.background = element_rect(colour = "grey"),
        plot.background = element_rect(color = "grey", fill = NA, size = 1))


p1
p2

overall_RMSE_richness <- calculate_RMSE(combined_pred_obs_richness$obs, combined_pred_obs_richness$pred)
overall_percent_RMSE_richness <- overall_RMSE_richness / mean(combined_pred_obs_richness$obs)
overall_R_squared_richness <- calculate_R_squared(combined_pred_obs_richness$obs, combined_pred_obs_richness$pred)

overall_RMSE_shannon <- calculate_RMSE(combined_pred_obs_shannon$obs, combined_pred_obs_shannon$pred)
overall_percent_RMSE_shannon <- overall_RMSE_shannon / mean(combined_pred_obs_shannon$obs)
overall_R_squared_shannon <- calculate_R_squared(combined_pred_obs_shannon$obs, combined_pred_obs_shannon$pred)
```