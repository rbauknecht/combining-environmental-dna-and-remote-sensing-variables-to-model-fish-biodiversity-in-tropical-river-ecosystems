combined_top_vars <- rbind(
transform(top_vars_maroni, Model = "Richness Maroni"),
transform(top_vars_maroni_shan, Model = "Shannon Maroni")
)
# Renaming variables to match them to ones actually described in paper
combined_top_vars <- combined_top_vars %>%
mutate(Variable = case_when(
Variable == "elevation" ~ "Elev",
Variable == "tsi" ~ "TSI",
Variable == "sd_ndvi" ~ "NDVI_std",
Variable == "ch" ~ "CH",
Variable == "sd_tsi" ~ "TSI_std",
Variable == "hum" ~ "HMI",
Variable == "rst" ~ "RST",
TRUE ~ Variable  # Retain any other values as they are
))
# Defining custom colours
my_colors <- c("CH" =  "#31688EFF", "Elev" = "#e31a1c", "HMI" = "#9ACD32",
"RST" = "#fada50", "TSI" = "#440154FF", "TSI_std" ="#d7e4f1"  ,
"sd_NDVI" =  "#35B779FF")
# Defining oder
variable_order <- c("RST","CH","NDVI_std","HMI","TSI_std","TSI","Elev")
combined_top_vars$Variable <- factor(combined_top_vars$Variable, levels = variable_order)
# Plotting with filled bars
ggplot(combined_top_vars, aes(x = Importance, y = Variable, fill = Variable)) +
geom_col(alpha=0.7, width=0.85) +
labs(title = "",
x = "Mean permutation MSE reduction",
y = "",
fill = "") +
scale_fill_manual(values = my_colors, guide = FALSE) +
facet_wrap(~ Model, scales = "free_y", ncol = 1) +
theme_minimal() +
theme(
axis.text.y = element_text(size = 8),
plot.margin = unit(c(0, 3, 0, 3), "cm"),
strip.text = element_blank(),
axis.title = element_text(size = 13),
axis.text  = element_text(size = 11),
axis.text.y.left = element_text(size = 10)
) +
guides(fill = FALSE)
#----------------------------------------------------------------------------------------------------#
# RESPONSE CURVES
#----------------------------------------------------------------------------------------------------#
# Preparing richness data
selected_df <- training_small_rf_maroni %>% select(all_of(selected_variables_maroni_richness))
training_frame <- as.data.frame(selected_df)
# Retrieving variable importance values for richness model
top_vars_maroni <- top_vars_maroni$Variable
pdp_results_richness <- list()
for (i in 1:length(top_vars_maroni)) {
variable_name <- top_vars_maroni[i]
pdp <- partialPlot(rf_maroni, pred.data = training_frame, x.var = paste(variable_name), plot = FALSE)
pdp_results_richness[[variable_name]] <- data.frame(Variable = rep(paste(variable_name), length(pdp$x)), x = pdp$x, y = pdp$y)
}
combined_pdp_richness <- do.call(rbind, pdp_results_richness)
combined_pdp_richness$plot_type <- "Richness"
# Preparing Shannon data
selected_df <- training_small_rf_maroni_shan %>% select(all_of(selected_variables_maroni_shannon))
training_frame <- as.data.frame(selected_df)
# Retrieving variable importance values for shannon model
top_vars_maroni_shan <- top_vars_maroni_shan$Variable
pdp_results_shannon <- list()
for (i in 1:length(top_vars_maroni_shan)) {
variable_name <- top_vars_maroni_shan[i]
pdp <- partialPlot(rf_maroni_shan, pred.data = training_frame, x.var = paste(variable_name), plot = FALSE)
pdp_results_shannon[[variable_name]] <- data.frame(Variable = rep(paste(variable_name), length(pdp$x)), x = pdp$x, y = pdp$y)
}
combined_pdp_shannon <- do.call(rbind, pdp_results_shannon)
combined_pdp_shannon$plot_type <- "Shannon Index"
# Combine both PDP results into a single data frame
combined_pdp <- rbind(combined_pdp_richness, combined_pdp_shannon)
# Adjusting the variable names
combined_pdp <- combined_pdp %>%
mutate(Variable = case_when(
Variable == "elevation" ~ "Elev",
Variable == "tsi" ~ "TSI",
Variable == "sd_ndvi" ~ "NDVI_std",
Variable == "ch" ~ "CH",
Variable == "sd_tsi" ~ "TSI_std",
Variable == "hum" ~ "HMI",
Variable == "rst" ~ "RST",
TRUE ~ Variable  # Retain any other values as they are
))
# Plotting response curves using facet_grid to place one plot above the other
ggplot(combined_pdp, aes(x = x, y = y, color = Variable)) +
geom_line() +
labs(title = "",
x = "Variable value",
y = "") +
scale_color_manual(values = my_colors) +  # Use defined colors
theme_minimal() +
facet_grid(plot_type ~ ., scales = "free_y") +
theme(
plot.margin = unit(c(0, 1, 0, 1), "cm"),
panel.border = element_rect(color = "black", fill = NA),
legend.box.background = element_rect(color = "black"),
axis.title = element_text(size = 13),
axis.text = element_text(size = 11),
legend.text = element_text(size = 12),
legend.title = element_text(size = 12),
strip.text = element_text(size = 12)
)
# Specify a vector of package names
packages_to_check <- c("randomForest","ggplot2","caret","tidyr", "tidyverse")
# Loop through each package
for (package_name in packages_to_check) {
# Check if the package is installed
if (!requireNamespace(package_name, quietly = TRUE)) {
# If not installed, install it
install.packages(package_name)
# Load the package
library(package_name, character.only = TRUE)
} else {
# If already installed, just load it
library(package_name, character.only = TRUE)
}
}
# Loading needed functions that are predefined in helper script
source("../scripts/helper_functions.R")
# Loading the data and separating the Oyapock river from the total frame
load("../R_data/combined_data_ind.Rdata")
data_oyapock <- subset(combined_data_ind, river == "oyapock")
# selecting only the columns containing the environmental variables in the data
data_oyapock_small <- data_oyapock %>% dplyr::select(-Sample, -Richness, -Shannon, -Simpson, -site, -Longitude, -Latitude, -river)
# Creating the correlation matrix
cor_matrix <- cor(data_oyapock_small)
# Finding highly correlated variables
highly_correlated <- which(cor_matrix > 0.75& cor_matrix < 1, arr.ind = TRUE)
# Create a data frame with correlated variables and their correlation values
correlation_table <- data.frame(
Variable1 = rownames(cor_matrix)[highly_correlated[, 1]],
Variable2 = colnames(cor_matrix)[highly_correlated[, 2]],
Correlation = cor_matrix[highly_correlated]
)
# Print the correlation table
print(correlation_table)
# Selecting variables to exclude in the further analysis based on the correlation outputs
variables_to_exclude <- c("elevation", "gpp", "sd_EVI", "sd_gpp", "sd_sd", "sd_ndvi", "sd_rst", "sd_slope", "sd_hum", "sd_ep", "sd", "sd_gpp")
set.seed(12345)  # For reproducibility
# Defining the fitting frame
training_frame <- data_oyapock
#----------------------------------------------------------------------------------------------------#
# PART 1: RICHNESS MODELLING
#----------------------------------------------------------------------------------------------------#
# Selecting only the predictor columns
training_small <- training_frame %>% dplyr::select(-Sample, -Shannon, -Simpson, -site, -Longitude, -Latitude, -river) %>% dplyr::select(-any_of(variables_to_exclude))
training_small_rf_oyapock <- training_small
# Setting up the RFE response and predictors
response <- training_small$Richness
predictors <- subset(training_small, select = -Richness)
# Setting up the control parameters for the RFE
ctrl <- rfeControl(functions=rfFuncs, method = "repeatedcv",
repeats = 5, number=5)
# Perform RFE with random forest
result <- rfe(predictors, response, sizes=c(1:ncol(predictors)), rfeControl=ctrl)
# Accessing the variables selected by RFE
selected_variables <- result$optVariables
selected_variables_oyapock_richness <- selected_variables
# Creating the formula to fit the models on based on RFE output
response_variable <- "Richness"
rf_form_oyapock <- as.formula(paste(response_variable, "~", paste(selected_variables, collapse = "+")))
# Training the richness Oyapock model, printing, and saving
rf_oyapock <- randomForest(rf_form_oyapock, data = training_frame, importance = T, type=regression, ntree=1000, mtry=4)
save(rf_oyapock, file = "../R_data/rf_oyapock.Rdata")
# Making predictions on the training data to evaluate the fit
prediction <- stats::predict(rf_oyapock, newdata = training_frame)
pred_obs_frame <- as.data.frame(cbind(pred=prediction,obs=as.numeric(as.character(training_frame$Richness))))
pred_obs_frame_richness_fitting <- pred_obs_frame
# Calculating the evaluation metrics on the prediction/observed values from the fitting
RMSE <- sqrt(mean((pred_obs_frame$obs - pred_obs_frame$pred)^2))
RMSE_richness_fitting <- RMSE
percent_RMSE <- RMSE/mean(training_frame$Richness)
percent_RMSE_richness_fitting <- percent_RMSE
# Calculating R-squared manually
mean_actual <- mean(pred_obs_frame$obs)
ss_total <- sum((pred_obs_frame$obs - mean_actual)^2)
ss_residual <- sum((pred_obs_frame$obs - pred_obs_frame$pred)^2)
r_squared <- 1 - (ss_residual / ss_total)
r_squared_richness_fitting <- r_squared
# Creating richness fitting predicted vs observed plot
limits <- c(0,151)
ggplot(data = pred_obs_frame, aes(x = pred, y = obs)) +
geom_point() +
labs(x = "Predicted Richness", y = "Observed Richness") +
ggtitle("") +
lims(x = limits, y = limits)+
guides(fill = "none")+
theme_minimal() +
theme(plot.margin = unit(c(0.3, 0.3, 0.3, 0.3), "cm"),
axis.text = element_text(size = 10),
axis.title.x = element_text(margin = margin(t = 13), size = 14),
axis.title.y = element_text(margin = margin(r = 13), size = 14),
plot.background = element_rect(color = "grey", fill = NA, size = 1))+
geom_abline(intercept = 0, slope = 1, linetype = "dashed", color = "blue")+
annotate("text", label = paste("RMSE =", round(RMSE, 2)), x = 132.5, y = 21, size = 4.5) +
annotate("text", label = paste("%RMSE =", round(percent_RMSE, 2)), x = 130.6, y = 12, size = 4.5) +
annotate("text", label = paste("R² =", sprintf("%.2f", round(r_squared, 2))), x = 137, y = 3, size = 4.5)
#----------------------------------------------------------------------------------------------------#
# PART 2: SHANNON INDEX MODELLING
#----------------------------------------------------------------------------------------------------#
# Selecting only the predictor columns of the data frame
training_small <- training_frame %>% dplyr::select(-Sample, -Richness, -Simpson, -site, -Longitude, -Latitude, -river)  %>% dplyr::select(-any_of(variables_to_exclude))
training_small_rf_oyapock_shan <- training_small
# Setting up RFE
response <- training_small$Shannon
predictors <- subset(training_small, select = -Shannon)
# Setting up the control parameters for the RFE
ctrl <- rfeControl(functions=rfFuncs, method = "repeatedcv",
repeats = 5, number=5)
# Perform RFE with random forest
result <- rfe(predictors, response, sizes=c(1:ncol(predictors)), rfeControl=ctrl)
# Accessing the variables selected by RFE
selected_variables <- result$optVariables
selected_variables_oyapock_shannon <- selected_variables
# Build formula based on selected variables
response_variable <- "Shannon"
rf_form_shan_oyapock <- as.formula(paste(response_variable, "~", paste(selected_variables, collapse = "+")))
# Training the shannon Oyapock model, printing, and saving
rf_oyapock_shan <- randomForest(rf_form_shan_oyapock, data = training_frame, importance = T, type=regression, ntree=1000, mtry=4)
save(rf_oyapock_shan, file = "../R_data/rf_oyapock_shan.Rdata")
# Making predictions on the training data to evaluate the fit
prediction  <- stats::predict(rf_oyapock_shan, newdata = training_frame)
pred_obs_frame <- as.data.frame(cbind(pred=prediction,obs=as.numeric(as.character(training_frame$Shannon))))
pred_obs_frame_shannon_fitting <- pred_obs_frame
# Calculating the evaluation metrics on the prediction/observed values from the fitting
RMSE <- sqrt(mean((pred_obs_frame$obs - pred_obs_frame$pred)^2))
RMSE_shannon_fitting <- RMSE
percent_RMSE <- RMSE/mean(training_frame$Shannon)
percent_RMSE_shannon_fitting <- percent_RMSE
# Calculating R-squared manually
mean_actual <- mean(pred_obs_frame$obs)
ss_total <- sum((pred_obs_frame$obs - mean_actual)^2)
ss_residual <- sum((pred_obs_frame$obs - pred_obs_frame$pred)^2)
r_squared <- 1 - (ss_residual / ss_total)
r_squared_shannon_fitting <- r_squared
# Creating shannon fitting predicted vs observed plot
limits <- c(0,5)
ggplot(data = pred_obs_frame, aes(x = pred, y = obs)) +
geom_point() +
labs(x = "Predicted Shannon Index", y = "Observed Shannon Index") +
ggtitle("") +
lims(x = limits, y = limits)+
guides(fill = "none")+
theme_minimal() +
theme(plot.margin = unit(c(0.3, 0.3, 0.3, 0.3), "cm"),
axis.text = element_text(size = 10),
axis.title.x = element_text(margin = margin(t = 13), size = 14),
axis.title.y = element_text(margin = margin(r = 13), size = 14),
plot.background = element_rect(color = "grey", fill = NA, size = 1))+
geom_abline(intercept = 0, slope = 1, linetype = "dashed", color = "blue")+
annotate("text", label = paste("RMSE =", round(RMSE, 2)), x = 4.5, y = 0.7, size = 4.5) +
annotate("text", label = paste("%RMSE =", round(percent_RMSE, 2)), x = 4.435, y = 0.4, size = 4.5) +
annotate("text", label = paste("R² =", sprintf("%.2f", round(r_squared, 2))), x = 4.64, y = 0.1, size = 4.5)
# Function to calculate RMSE
calculate_RMSE <- function(observed, predicted) {
sqrt(mean((observed - predicted)^2))
}
# Function to calculate R-squared
calculate_R_squared <- function(observed, predicted) {
mean_actual <- mean(observed)
ss_total <- sum((observed - mean_actual)^2)
ss_residual <- sum((observed - predicted)^2)
1 - (ss_residual / ss_total)
}
# Function to perform RFE
perform_RFE <- function(predictors, response) {
ctrl <- rfeControl(functions=rfFuncs, method = "repeatedcv", repeats = 5, number=5)
result <- rfe(predictors, response, sizes=c(1:ncol(predictors)), rfeControl=ctrl)
selected_variables <- result$optVariables
return(selected_variables)
}
# Randomly shuffle row indices
set.seed(12345)  # For reproducibility
random_indices <- sample(nrow(data_oyapock))
# Randomly splitting data into 5 folds
folds <- cut(seq(1, nrow(data_oyapock)), breaks = 5, labels = FALSE)
# Initialize vectors to store results
combined_predictions_richness <- NULL
combined_predictions_shannon <- NULL
combined_observed_richness <- NULL
combined_observed_shannon <- NULL
for (i in 1:5) {
# Creating training and testing sets for each fold
testing_indices <- which(folds == i, arr.ind = TRUE)
testing_frame <- data_oyapock[random_indices[testing_indices], ]
training_frame <- data_oyapock[random_indices[-testing_indices], ]
# Selecting predictors
training_small_richness <- training_frame %>%
dplyr::select(-Sample, -Shannon, -Simpson, -site, -Longitude, -Latitude, -river) %>%
dplyr::select(-any_of(variables_to_exclude))
training_small_shannon <- training_frame %>%
dplyr::select(-Sample, -Richness, -Simpson, -site, -Longitude, -Latitude, -river) %>%
dplyr::select(-any_of(variables_to_exclude))
# Set up response and predictors
response_richness <- training_small_richness$Richness
predictors_richness <- subset(training_small_richness, select = -Richness)
response_shannon <- training_small_shannon$Shannon
predictors_shannon <- subset(training_small_shannon, select = -Shannon)
# Training models
rf_model_richness <- randomForest(response_richness ~ ., data = predictors_richness, importance = TRUE, ntree = 1000, mtry = 4)
rf_model_shannon <- randomForest(response_shannon ~ ., data = predictors_shannon, importance = TRUE, ntree = 1000, mtry = 4)
# Making predictions on testing data
predictions_richness <- predict(rf_model_richness, newdata = testing_frame)
predictions_shannon <- predict(rf_model_shannon, newdata = testing_frame)
# Combine predictions and observed values
combined_predictions_richness <- c(combined_predictions_richness, predictions_richness)
combined_observed_richness <- c(combined_observed_richness, testing_frame$Richness)
combined_predictions_shannon <- c(combined_predictions_shannon, predictions_shannon)
combined_observed_shannon <- c(combined_observed_shannon, testing_frame$Shannon)
}
# Create combined data frame for plotting
pred_obs_frame_richness <- data.frame(pred = combined_predictions_richness, obs = combined_observed_richness)
pred_obs_frame_shannon <- data.frame(pred = combined_predictions_shannon, obs = combined_observed_shannon)
# Calculate overall performance metrics
overall_RMSE_richness <- calculate_RMSE(pred_obs_frame_richness$obs, pred_obs_frame_richness$pred)
overall_percent_RMSE_richness <- overall_RMSE_richness / mean(pred_obs_frame_richness$obs)
overall_R_squared_richness <- calculate_R_squared(pred_obs_frame_richness$obs, pred_obs_frame_richness$pred)
overall_RMSE_shannon <- calculate_RMSE(pred_obs_frame_shannon$obs, pred_obs_frame_shannon$pred)
overall_percent_RMSE_shannon <- overall_RMSE_shannon / mean(pred_obs_frame_shannon$obs)
overall_R_squared_shannon <- calculate_R_squared(pred_obs_frame_shannon$obs, pred_obs_frame_shannon$pred)
# Creating predicted vs observed plot for richness
limits <- c(0, 151)
ggplot(data = pred_obs_frame_richness, aes(x = pred, y = obs)) +
geom_point() +
labs(x = "Predicted Richness", y = "Observed Richness") +
ggtitle("") +
lims(x = limits, y = limits) +
guides(fill = "none") +
theme_minimal() +
theme(plot.margin = unit(c(0.3, 0.3, 0.3, 0.3), "cm"),
axis.text = element_text(size = 10),
axis.title.x = element_text(margin = margin(t = 13), size=14),
axis.title.y = element_text(margin = margin(r = 13), size=14),
plot.background = element_rect(color = "grey", fill = NA, size = 1))+
geom_abline(intercept = 0, slope = 1, linetype = "dashed", color = "blue") +
annotate("text", label = paste("RMSE =", round(overall_RMSE_richness, 2)), x = 133.8, y = 21, size = 4.5) +
annotate("text", label = paste("%RMSE =", round(overall_percent_RMSE_richness, 2)), x = 130.6, y = 12, size = 4.5) +
annotate("text", label = paste("R² =", sprintf("%.2f", round(overall_R_squared_richness, 2))), x = 137, y = 3, size = 4.5)
# Creating predicted vs observed plot for shannon
limits <- c(0,5)
ggplot(data = pred_obs_frame_shannon, aes(x = pred, y = obs)) +
geom_point() +
labs(x = "Predicted Shannon Index", y = "Observed Shannon Index") +
ggtitle("") +
lims(x = limits, y = limits) +
guides(fill = "none") +
theme_minimal() +
theme(plot.margin = unit(c(0.3, 0.3, 0.3, 0.3), "cm"),
axis.text = element_text(size = 10),
axis.title.x = element_text(margin = margin(t = 13), size=14),
axis.title.y = element_text(margin = margin(r = 13), size=14),
plot.background = element_rect(color = "grey", fill = NA, size = 1))+
geom_abline(intercept = 0, slope = 1, linetype = "dashed", color = "blue") +
annotate("text", label = paste("RMSE =", round(overall_RMSE_shannon, 2)), x = 4.51, y = 0.7, size = 4.5) +
annotate("text", label = paste("%RMSE =", round(overall_percent_RMSE_shannon, 2)), x = 4.436, y = 0.4, size = 4.5) +
annotate("text", label = paste("R² =", sprintf("%.2f", round(overall_R_squared_shannon, 2))), x = 4.64, y = 0.1, size = 4.5)
#----------------------------------------------------------------------------------------------------#
# VARIABLE IMPORTANCE
#----------------------------------------------------------------------------------------------------#
# Extracting variable importance for richness model
importance_values_oyapock <- importance(rf_oyapock)
var_importance_df_oyapock <- data.frame(
Variable = rownames(importance_values_oyapock),
Importance = importance_values_oyapock[, 1],
Model = "Richness Oyapock"
)
# Extracting variable importance for shannon model
importance_values_oyapock_shan <- importance(rf_oyapock_shan)
var_importance_df_oyapock_shan <- data.frame(
Variable = rownames(importance_values_oyapock_shan),
Importance = importance_values_oyapock_shan[, 1],
Model = "Shannon Oyapock"
)
# Sorting the data frames by importance in descending order
var_importance_df_oyapock <- var_importance_df_oyapock[order(-var_importance_df_oyapock$Importance), ]
var_importance_df_oyapock_shan <- var_importance_df_oyapock_shan[order(-var_importance_df_oyapock_shan$Importance), ]
# Taking the top 5 variables for each model
top_vars_oyapock <- head(var_importance_df_oyapock, 5)
top_vars_oyapock_shan <- head(var_importance_df_oyapock_shan, 5)
# Combine the top variables from both models into a single data frame
combined_top_vars <- rbind(
transform(top_vars_oyapock, Model = "Richness Oyapock"),
transform(top_vars_oyapock_shan, Model = "Shannon Oyapock")
)
# Renaming variables to match them to ones actually described in paper
combined_top_vars <- combined_top_vars %>%
mutate(Variable = case_when(
Variable == "sd_elevation" ~ "Elev_std",
Variable == "slope" ~ "Slope",
Variable == "hum" ~ "HMI",
Variable == "tsi" ~ "TSI",
Variable == "sum" ~ "WaterA",
Variable == "ch" ~ "CH",
Variable == "ep" ~ "EP",
TRUE ~ Variable  # Retain any other values as they are
))
# Defining custom colours
my_colors <- c("CH" =  "#31688EFF", "Slope" = "#e31a1c", "HMI" = "#9ACD32",
"Elev_std" = "#fada50", "TSI" = "#440154FF", "EP" ="#d7e4f1"  ,
"WaterA" =  "#35B779FF")
# Defining oder
variable_order <- c("CH","EP","WaterA","TSI","HMI","Slope","Elev_std")
combined_top_vars$Variable <- factor(combined_top_vars$Variable, levels = variable_order)
# Plotting with filled bars
ggplot(combined_top_vars, aes(x = Importance, y = Variable, fill = Variable)) +
geom_col(alpha=0.7, width=0.85) +
labs(title = "",
x = "Mean permutation MSE reduction",
y = "",
fill = "") +
scale_fill_manual(values = my_colors, guide = FALSE) +
facet_wrap(~ Model, scales = "free_y", ncol = 1) +
theme_minimal() +
theme(
axis.text.y = element_text(size = 8),
plot.margin = unit(c(0, 3, 0, 3), "cm"),
strip.text = element_blank(),
axis.title = element_text(size = 13),
axis.text  = element_text(size = 11),
axis.text.y.left = element_text(size = 10)
) +
guides(fill = FALSE)
#----------------------------------------------------------------------------------------------------#
# RESPONSE CURVES
#----------------------------------------------------------------------------------------------------#
# Preparing richness data
selected_df <- training_small_rf_oyapock %>% select(all_of(selected_variables_oyapock_richness))
training_frame <- as.data.frame(selected_df)
# Retrieving variable importance values for richness model
top_vars_oyapock <- top_vars_oyapock$Variable
pdp_results_richness <- list()
for (i in 1:length(top_vars_oyapock)) {
variable_name <- top_vars_oyapock[i]
pdp <- partialPlot(rf_oyapock, pred.data = training_frame, x.var = paste(variable_name), plot = FALSE)
pdp_results_richness[[variable_name]] <- data.frame(Variable = rep(paste(variable_name), length(pdp$x)), x = pdp$x, y = pdp$y)
}
combined_pdp_richness <- do.call(rbind, pdp_results_richness)
combined_pdp_richness$plot_type <- "Richness"
# Preparing Shannon data
selected_df <- training_small_rf_oyapock_shan %>% select(all_of(selected_variables_oyapock_shannon))
training_frame <- as.data.frame(selected_df)
# Retrieving variable importance values for shannon model
top_vars_oyapock_shan <- top_vars_oyapock_shan$Variable
pdp_results_shannon <- list()
for (i in 1:length(top_vars_oyapock_shan)) {
variable_name <- top_vars_oyapock_shan[i]
pdp <- partialPlot(rf_oyapock_shan, pred.data = training_frame, x.var = paste(variable_name), plot = FALSE)
pdp_results_shannon[[variable_name]] <- data.frame(Variable = rep(paste(variable_name), length(pdp$x)), x = pdp$x, y = pdp$y)
}
combined_pdp_shannon <- do.call(rbind, pdp_results_shannon)
combined_pdp_shannon$plot_type <- "Shannon Index"
# Combine both PDP results into a single data frame
combined_pdp <- rbind(combined_pdp_richness, combined_pdp_shannon)
# Adjusting the variable names
combined_pdp <- combined_pdp %>%
mutate(Variable = case_when(
Variable == "sd_elevation" ~ "Elev_std",
Variable == "slope" ~ "Slope",
Variable == "hum" ~ "HMI",
Variable == "tsi" ~ "TSI",
Variable == "sum" ~ "WaterA",
Variable == "ch" ~ "CH",
Variable == "ep" ~ "EP",
TRUE ~ Variable  # Retain any other values as they are
))
# Plotting response curves using facet_grid to place one plot above the other
ggplot(combined_pdp, aes(x = x, y = y, color = Variable)) +
geom_line() +
labs(title = "",
x = "Variable value",
y = "") +
scale_color_manual(values = my_colors) +  # Use defined colors
theme_minimal() +
facet_grid(plot_type ~ ., scales = "free_y") +
theme(
plot.margin = unit(c(0, 1, 0, 1), "cm"),
panel.border = element_rect(color = "black", fill = NA),
legend.box.background = element_rect(color = "black"),
axis.title = element_text(size = 13),
axis.text = element_text(size = 11),
legend.text = element_text(size = 12),
legend.title = element_text(size = 12),
strip.text = element_text(size = 12)
)
# Specify a vector of package names
packages_to_check <- c("randomForest","ggplot2","caret","tidyr", "tidyverse")
# Loop through each package
for (package_name in packages_to_check) {
# Check if the package is installed
if (!requireNamespace(package_name, quietly = TRUE)) {
# If not installed, install it
install.packages(package_name)
# Load the package
library(package_name, character.only = TRUE)
} else {
# If already installed, just load it
library(package_name, character.only = TRUE)
}
}
# Loading needed functions that are predefined in helper script
source("../scripts/helper_functions.R")
# Loading the data and separating the Maroni river from the total frame
load("../R_data/combined_data.Rdata")
data_maroni <- subset(combined_data, river == "maroni")
data_oyapock <- subset(combined_data, river == "oyapock")
data_kinabatangan <- subset(combined_data, river == "kinabatangan")
data_magdalena <- subset(combined_data, river == "magdalena")
data_okavango <- subset(combined_data, river == "okavango")
data_casamance <- subset(combined_data, river == "casamance")
View(data_okavango)
