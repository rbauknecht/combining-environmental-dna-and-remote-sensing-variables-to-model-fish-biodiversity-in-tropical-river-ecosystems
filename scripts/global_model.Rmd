---
title: "global_model"
author: "Robin Bauknecht"
date: "`r Sys.Date()`"
output: html_document
---

## This script contains the analysis for the global models.

------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Loading packages and data

- Here all the packages used in the analysis are installed if needed and then loaded. 
- Also helper functions of the script "../scripts/helper_functions.R" are sourced to be available.
- Combined data containing biodiversity and remote sensing information is loaded for every river.

```{r, message=FALSE, warning=FALSE}
# Specify a vector of package names
packages_to_check <- c("randomForest","ggplot2","caret","tidyr", "tidyverse")

# Loop through each package
for (package_name in packages_to_check) {
  # Check if the package is installed
  if (!requireNamespace(package_name, quietly = TRUE)) {
    # If not installed, install it
    install.packages(package_name)
    
    # Load the package
    library(package_name, character.only = TRUE)
  } else {
    # If already installed, just load it
    library(package_name, character.only = TRUE)
  }
}

# Loading needed functions that are predefined in helper script
source("../scripts/helper_functions.R")

# Loading the data and separating the Maroni river from the total frame 
load("../R_data/combined_data.Rdata")
data_maroni <- subset(combined_data, river == "maroni")
data_oyapock <- subset(combined_data, river == "oyapock")
data_kinabatangan <- subset(combined_data, river == "kinabatangan")
data_magdalena <- subset(combined_data, river == "magdalena")
data_okavango <- subset(combined_data, river == "okavango")
data_casamance <- subset(combined_data, river == "casamance")
```

------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Testing for correlation between remote sensing predictiors and making preselection

- Here we are testing for correlations between the predictors and making a selection of variables to use in the models
- Pearson correlation is used as metric

```{r}
# Selecting only the columns containing the environmental variables in the data
non_variables <- c("Sample", "Richness", "Shannon", "Simpson", "site", "Longitude", "Latitude", "river")
combined_data_small <- combined_data %>% dplyr::select(-any_of(non_variables))

# Creating the correlation matrix
cor_matrix <- cor(combined_data_small)

# Determining highly correlated variables
highly_correlated <- which(cor_matrix > 0.75& cor_matrix < 1, arr.ind = TRUE)

# Creating a data frame with correlated variables and their correlation values
correlation_table <- data.frame(
  Variable1 = rownames(cor_matrix)[highly_correlated[, 1]],
  Variable2 = colnames(cor_matrix)[highly_correlated[, 2]],
  Correlation = cor_matrix[highly_correlated]
)

# Printing the correlation table
print(correlation_table)

# Selecting variables to exclude in the further analysis based on the correlation outputs
variables_to_exclude <- c("EVI", "ep", "sd_hum", "sd_sd", "sd_slope", "sd_tsi", "gpp", "ndvi")
```

------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Model fitting and evaluation of the fit

- Here all the data is used to fit models and evaluate this fit. 
- This gives insights into how well the variables are able to capture the ecological patterns of the fish distribution. 
- Then predicted vs observed plots are added to visually evaluate the fit of the models.

```{r}
set.seed(12345)  # For reproducibility

# Defining combined fitting data
training_frame <- rbind(data_maroni, data_oyapock, data_kinabatangan, data_casamance, data_magdalena, data_okavango)

#----------------------------------------------------------------------------------------------------#
# PART 1: RICHNESS MODELLING 
#----------------------------------------------------------------------------------------------------#

# Selecting only the predictor columns of the combined data frame
training_small <- training_frame %>% dplyr::select(-Sample, -Shannon, -Simpson, -site, -Longitude, -Latitude, -river) %>% dplyr::select(-any_of(variables_to_exclude))
training_small_rf_global <- training_small

# Setting up the RFE response and predictors
response <- training_small$Richness
predictors <- subset(training_small, select = -Richness)

# Setting up the control parameters for the RFE
ctrl <- rfeControl(functions=rfFuncs, method = "repeatedcv", repeats = 5, number=5)

# Performing RFE with random forest
result <- rfe(predictors, response, sizes=c(1:ncol(predictors)), rfeControl=ctrl)# Display the results

# Accessing the variables selected by RFE
selected_variables <- result$optVariables
selected_variables_global_richness <- selected_variables

# Creating the formula to fit the models on based on RFE output
response_variable <- "Richness"  
rf_form <- as.formula(paste(response_variable, "~", paste(selected_variables, collapse = "+")))

# Training the richness global model, printing, and saving 
rf_global <- randomForest(rf_form, data = training_frame, importance = T, type=regression, ntree=1000, mtry=4)
rf_global
save(rf_global, file = "../R_data/rf_global.Rdata")

# Making predictions on the training data to evaluate the fit
prediction  <- stats::predict(rf_global, newdata = training_frame)
pred_obs_frame <- as.data.frame(cbind(pred=prediction,obs=as.numeric(as.character(training_frame$Richness))))
pred_obs_frame_richness_fitting <- pred_obs_frame

# Calculating the evaluation metrics on the prediction/observed values from the fitting
RMSE <- sqrt(mean((pred_obs_frame$obs - pred_obs_frame$pred)^2))
RMSE_richness_fitting <- RMSE
percent_RMSE <- RMSE/mean(training_frame$Richness)
percent_RMSE_richness_fitting <- percent_RMSE
# Calculating R-squared manually
mean_actual <- mean(pred_obs_frame$obs)
ss_total <- sum((pred_obs_frame$obs - mean_actual)^2)
ss_residual <- sum((pred_obs_frame$obs - pred_obs_frame$pred)^2)
r_squared <- 1 - (ss_residual / ss_total)
r_squared_richness_fitting <- r_squared

# Creating richness fitting predicted vs observed plot
limits <- c(0,151)
ggplot(data = pred_obs_frame, aes(x = pred, y = obs)) +
  geom_point() +
  labs(x = "Predicted Richness", y = "Observed Richness") +
  ggtitle("") +
 lims(x = limits, y = limits)+
  geom_abline(intercept = 0, slope = 1, linetype = "dashed", color = "blue") +
  guides(fill = "none")+  
  theme_minimal() +
  theme(plot.margin = unit(c(0.3, 0.3, 0.3, 0.3), "cm"),
        axis.text = element_text(size = 10),
        axis.title.x = element_text(margin = margin(t = 13), size = 14),
        axis.title.y = element_text(margin = margin(r = 13), size = 14),
        plot.background = element_rect(color = "grey", fill = NA, size = 1))+
  annotate("text", label = paste("RMSE =", sprintf("%.2f", RMSE)), x = 132.5, y = 21, size = 4.5) +
  annotate("text", label = paste("%RMSE =", sprintf("%.2f", percent_RMSE)), x = 130.55, y = 12, size = 4.5) +
  annotate("text", label = paste("R² =", sprintf("%.2f", r_squared)), x = 137, y = 3, size = 4.5)

#----------------------------------------------------------------------------------------------------#
# PART 2: SHANNON INDEX MODELLING 
#----------------------------------------------------------------------------------------------------#

# Selecting only the predictor columns of the combined data frame
training_small <- training_frame %>% dplyr::select(-Sample, -Richness, -Simpson, -site, -Longitude, -Latitude, -river) %>% dplyr::select(-any_of(variables_to_exclude))
training_small_rf_global_shan <- training_small

# Setting up RFE
response <- training_small$Shannon
predictors <- subset(training_small, select = -Shannon)

# Setting up the control parameters for the RFE
ctrl <- rfeControl(functions=rfFuncs, method = "repeatedcv", repeats = 5, number=5)

# Performing RFE with random forest
result <- rfe(predictors, response, sizes=c(1:ncol(predictors)), rfeControl=ctrl)

# Accessing the variables selected by RFE
selected_variables <- result$optVariables
selected_variables_global_shannon <- selected_variables

# Creating the formula to fit the models on based on RFE output
response_variable <- "Shannon"  
rf_form_shan <- as.formula(paste(response_variable, "~", paste(selected_variables, collapse = "+")))

# Training the richness global model, printing, and saving 
rf_global_shan <- randomForest(rf_form_shan, data = training_frame, importance = T, type=regression, ntree=1000, mtry=4)
rf_global_shan
save(rf_global_shan, file = "../R_data/rf_global_shan.Rdata")

# Making predictions on the training data to evaluate the fit
prediction  <- stats::predict(rf_global_shan, newdata = training_frame)
pred_obs_frame <- as.data.frame(cbind(pred=prediction,obs=as.numeric(as.character(training_frame$Shannon))))
pred_obs_frame_shannon_fitting <- pred_obs_frame

# Calculating the evaluation metrics on the prediction/observed values from the fitting
RMSE <- sqrt(mean((pred_obs_frame$obs - pred_obs_frame$pred)^2))
RMSE_shannon_fitting <- RMSE
percent_RMSE <- RMSE/mean(training_frame$Shannon)
percent_RMSE_shannon_fitting <- percent_RMSE
# Calculating R-squared manually
mean_actual <- mean(pred_obs_frame$obs)
ss_total <- sum((pred_obs_frame$obs - mean_actual)^2)
ss_residual <- sum((pred_obs_frame$obs - pred_obs_frame$pred)^2)
r_squared <- 1 - (ss_residual / ss_total)
r_squared <- round(r_squared, 2)
r_squared_shannon_fitting <- r_squared

# Creating richness fitting predicted vs observed plot
limits <- c(0,5)
ggplot(data = pred_obs_frame, aes(x = pred, y = obs)) +
  geom_point() +
  labs(x = "Predicted Shannon index", y = "Observed Shannon index") +
  ggtitle("") +
 lims(x = limits, y = limits)+
  geom_abline(intercept = 0, slope = 1, linetype = "dashed", color = "blue") +
  guides(fill = "none")+  
  theme_minimal() +
  theme(plot.margin = unit(c(0.3, 0.3, 0.3, 0.3), "cm"),
        axis.text = element_text(size = 10),
        axis.title.x = element_text(margin = margin(t = 13), size = 14),
        axis.title.y = element_text(margin = margin(r = 13), size = 14),
        plot.background = element_rect(color = "grey", fill = NA, size = 1))+
  annotate("text", label = paste("RMSE =", sprintf("%.2f", RMSE)), x = 4.5, y = 0.7, size = 4.5) +
  annotate("text", label = paste("%RMSE =", sprintf("%.2f", percent_RMSE)), x = 4.432, y = 0.4, size = 4.5) +
  annotate("text", label = paste("R² =", sprintf("%.2f", r_squared)), x = 4.635, y = 0.1, size = 4.5) 
```

------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Cross valiation to assess the predictive capabilities of the models

- Now we are performing 5 fold cross validation to evaluate the predictive capabilities of the model. 

```{r}
# Function to calculate RMSE
calculate_RMSE <- function(observed, predicted) {
  sqrt(mean((observed - predicted)^2))
}

# Function to calculate R-squared
calculate_R_squared <- function(observed, predicted) {
  mean_actual <- mean(observed)
  ss_total <- sum((observed - mean_actual)^2)
  ss_residual <- sum((observed - predicted)^2)
  1 - (ss_residual / ss_total)
}

# Randomly shuffling row indices for each dataset
set.seed(12345)  # For reproducibility
random_indices_oyapock <- sample(nrow(data_oyapock))
random_indices_maroni <- sample(nrow(data_maroni))
random_indices_casamance <- sample(nrow(data_casamance))
random_indices_magdalena <- sample(nrow(data_magdalena))
random_indices_kinabatangan <- sample(nrow(data_kinabatangan))
random_indices_okavango <- sample(nrow(data_okavango))

# Randomly splitting data into 5 folds for each dataset
folds_oyapock <- cut(seq(1, nrow(data_oyapock)), breaks = 5, labels = FALSE)
folds_maroni <- cut(seq(1, nrow(data_maroni)), breaks = 5, labels = FALSE)
folds_casamance <- cut(seq(1, nrow(data_casamance)), breaks = 5, labels = FALSE)
folds_magdalena <- cut(seq(1, nrow(data_magdalena)), breaks = 5, labels = FALSE)
folds_kinabatangan <- cut(seq(1, nrow(data_kinabatangan)), breaks = 5, labels = FALSE)
folds_okavango <- cut(seq(1, nrow(data_okavango)), breaks = 5, labels = FALSE)

# Initializing vectors to store results
RMSE_values_richness <- numeric(5)
RMSE_values_shannon <- numeric(5)
percent_RMSE_values_richness <- numeric(5)
percent_RMSE_values_shannon <- numeric(5)
R_squared_values_richness <- numeric(5)
R_squared_values_shannon <- numeric(5)
combined_predictions_richness <- NULL
combined_predictions_shannon <- NULL
combined_observed_richness <- NULL
combined_observed_shannon <- NULL

for (i in 1:5) {
  # Creating training and testing sets for each fold for each dataset
  testing_indices_oyapock <- which(folds_oyapock == i, arr.ind = TRUE)
  testing_frame_oyapock <- data_oyapock[random_indices_oyapock[testing_indices_oyapock], ]
  training_frame_oyapock <- data_oyapock[random_indices_oyapock[-testing_indices_oyapock], ]
  
  testing_indices_maroni <- which(folds_maroni == i, arr.ind = TRUE)
  testing_frame_maroni <- data_maroni[random_indices_maroni[testing_indices_maroni], ]
  training_frame_maroni <- data_maroni[random_indices_maroni[-testing_indices_maroni], ]
  
  testing_indices_casamance <- which(folds_casamance == i, arr.ind = TRUE)
  testing_frame_casamance <- data_casamance[random_indices_casamance[testing_indices_casamance], ]
  training_frame_casamance <- data_casamance[random_indices_casamance[-testing_indices_casamance], ]
  
  testing_indices_magdalena <- which(folds_magdalena == i, arr.ind = TRUE)
  testing_frame_magdalena <- data_magdalena[random_indices_magdalena[testing_indices_magdalena], ]
  training_frame_magdalena <- data_magdalena[random_indices_magdalena[-testing_indices_magdalena], ]
  
  testing_indices_kinabatangan <- which(folds_kinabatangan == i, arr.ind = TRUE)
  testing_frame_kinabatangan <- data_kinabatangan[random_indices_kinabatangan[testing_indices_kinabatangan], ]
  training_frame_kinabatangan <- data_kinabatangan[random_indices_kinabatangan[-testing_indices_kinabatangan], ]
  
  testing_indices_okavango <- which(folds_okavango == i, arr.ind = TRUE)
  testing_frame_okavango <- data_okavango[random_indices_okavango[testing_indices_okavango], ]
  training_frame_okavango <- data_okavango[random_indices_okavango[-testing_indices_okavango], ]
  
  # Combining training frames for all datasets
  training_frame <- rbind(training_frame_oyapock, training_frame_maroni, training_frame_casamance,
                          training_frame_magdalena, training_frame_kinabatangan, training_frame_okavango)
  
  # Combining testing frames for all datasets
  testing_frame <- rbind(testing_frame_oyapock, testing_frame_maroni, testing_frame_casamance,
                         testing_frame_magdalena, testing_frame_kinabatangan, testing_frame_okavango)
  
  # Selecting only predictor variables
  training_small_richness <- training_frame %>% 
    dplyr::select(-Sample, -Shannon, -Simpson, -site, -Longitude, -Latitude, -river) %>% 
    dplyr::select(-any_of(variables_to_exclude))
  training_small_shannon <- training_frame %>% 
    dplyr::select(-Sample, -Richness, -Simpson, -site, -Longitude, -Latitude, -river) %>% 
    dplyr::select(-any_of(variables_to_exclude))
  
  # Setting up response and predictors
  response_richness <- training_small_richness$Richness
  predictors_richness <- subset(training_small_richness, select = -Richness)
  response_shannon <- training_small_shannon$Shannon
  predictors_shannon <- subset(training_small_shannon, select = -Shannon)
  
  # Training the richness and shannon models
  rf_model_richness <- randomForest(response_richness ~ ., data = predictors_richness, importance = TRUE, ntree = 1000, mtry = 4)
  rf_model_shannon <- randomForest(response_shannon ~ ., data = predictors_shannon, importance = TRUE, ntree = 1000, mtry = 4)
  
  # Making predictions on the testing data
  predictions_richness <- predict(rf_model_richness, newdata = testing_frame)
  predictions_shannon <- predict(rf_model_shannon, newdata = testing_frame)
  
  # Combining predictions and observed values
  combined_predictions_richness <- c(combined_predictions_richness, predictions_richness)
  combined_observed_richness <- c(combined_observed_richness, testing_frame$Richness)
  combined_predictions_shannon <- c(combined_predictions_shannon, predictions_shannon)
  combined_observed_shannon <- c(combined_observed_shannon, testing_frame$Shannon)
}

# Create combined data frame of the results for plotting
pred_obs_frame_richness <- data.frame(pred = combined_predictions_richness, obs = combined_observed_richness)
pred_obs_frame_shannon <- data.frame(pred = combined_predictions_shannon, obs = combined_observed_shannon)

# Calculating overall performance metrics
overall_RMSE_richness <- calculate_RMSE(pred_obs_frame_richness$obs, pred_obs_frame_richness$pred)
overall_percent_RMSE_richness <- overall_RMSE_richness / mean(pred_obs_frame_richness$obs)
overall_R_squared_richness <- calculate_R_squared(pred_obs_frame_richness$obs, pred_obs_frame_richness$pred)
overall_RMSE_shannon <- calculate_RMSE(pred_obs_frame_shannon$obs, pred_obs_frame_shannon$pred)
overall_percent_RMSE_shannon <- overall_RMSE_shannon / mean(pred_obs_frame_shannon$obs)
overall_R_squared_shannon <- calculate_R_squared(pred_obs_frame_shannon$obs, pred_obs_frame_shannon$pred)

# Creating predicted vs observed plot for richness
limits_richness <- c(0, 151)
ggplot(data = pred_obs_frame_richness, aes(x = pred, y = obs)) +
  geom_point() +
  labs(x = "Predicted Richness", y = "Observed Richness") +
  ggtitle("") +
  lims(x = limits_richness, y = limits_richness) +
  guides(fill = "none") +
  theme_minimal() +
    theme(plot.margin = unit(c(0.3, 0.3, 0.3, 0.3), "cm"),
          axis.text = element_text(size = 10),
       axis.title.x = element_text(margin = margin(t = 13), size=14),
      axis.title.y = element_text(margin = margin(r = 13), size=14),
        plot.background = element_rect(color = "grey", fill = NA, size = 1))+
  geom_abline(intercept = 0, slope = 1, linetype = "dashed", color = "blue") +
  annotate("text", label = paste("RMSE =", round(overall_RMSE_richness, 2)), x = 133.8, y = 21, size = 4.5) +
  annotate("text", label = paste("%RMSE =", round(overall_percent_RMSE_richness, 2)), x = 130.55, y = 12, size = 4.5) +
  annotate("text", label = paste("R² =", sprintf("%.2f", round(overall_R_squared_richness, 2))), x = 137, y = 3, size = 4.5)

# Creating predicted vs observed plot for shannon
limits_shannon <- c(0,5)
ggplot(data = pred_obs_frame_shannon, aes(x = pred, y = obs)) +
  geom_point() +
  labs(x = "Predicted Shannon Index", y = "Observed Shannon Index") +
  ggtitle("") +
  lims(x = limits_shannon, y = limits_shannon) +
  guides(fill = "none") +
  theme_minimal() +
    theme(plot.margin = unit(c(0.3, 0.3, 0.3, 0.3), "cm"),
         axis.text = element_text(size = 10),
        axis.title.x = element_text(margin = margin(t = 13), size=14),
        axis.title.y = element_text(margin = margin(r = 13), size=14),
        plot.background = element_rect(color = "grey", fill = NA, size = 1))+
  geom_abline(intercept = 0, slope = 1, linetype = "dashed", color = "blue") +
  annotate("text", label = paste("RMSE =", round(overall_RMSE_shannon, 2)), x = 4.5, y = 0.7, size = 4.5) +
  annotate("text", label = paste("%RMSE =", round(overall_percent_RMSE_shannon, 2)), x = 4.432, y = 0.4, size = 4.5) +
  annotate("text", label = paste("R² =", sprintf("%.2f", round(overall_R_squared_shannon, 2))), x = 4.635, y = 0.1, size = 4.5)
```

------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Geographic model evaluation

- Here we assess transferability of the models to unencountered rivers by always training on 5/6 rivers and predicting on one left out

```{r}
# Creating a list of all rivers
frame_list <- list(data_casamance, data_kinabatangan, data_magdalena, data_oyapock, data_maroni, data_okavango)
river_names <- c("casamance", "kinabatangan", "magdalena", "oyapock", "maroni", "okavango")

# Looping over each river
for (i in 1:6) {
  
  # Assigning the testing river
  testing_river <- frame_list[[i]]
  
  # Combining the remaining rivers to create the training set
  training_rivers <- frame_list[-i]
  training_frame <- do.call(rbind, training_rivers)
  
  # Selecting only the predictor columns
  training_small_richness <- training_frame %>% 
    dplyr::select(-Sample, -Shannon, -Simpson, -site, -Longitude, -Latitude, -river) %>% 
    dplyr::select(-any_of(variables_to_exclude))
  training_small_shannon <- training_frame %>% 
    dplyr::select(-Sample, -Richness, -Simpson, -site, -Longitude, -Latitude, -river) %>% 
    dplyr::select(-any_of(variables_to_exclude))
  
  # Setting up response and predictors
  response_richness <- training_small_richness$Richness
  predictors_richness <- subset(training_small_richness, select = -Richness)
  response_shannon <- training_small_shannon$Shannon
  predictors_shannon <- subset(training_small_shannon, select = -Shannon)
  
  # Training the random forest models
  rf_model_richness <- randomForest(response_richness ~ ., data = predictors_richness, importance = TRUE, ntree = 1000, mtry = 4)
  rf_model_shannon <- randomForest(response_shannon ~ ., data = predictors_shannon, importance = TRUE, ntree = 1000, mtry = 4)
  
  # Preparing the testing data
  testing_small_richness <- testing_river %>% 
    dplyr::select(-Sample, -Shannon, -Simpson, -site, -Longitude, -Latitude, -river) %>% 
    dplyr::select(-any_of(variables_to_exclude))
  testing_small_shannon <- testing_river %>% 
    dplyr::select(-Sample, -Richness, -Simpson, -site, -Longitude, -Latitude, -river) %>% 
    dplyr::select(-any_of(variables_to_exclude))
  
  # Predicting on the testing data
  predictions_richness <- predict(rf_model_richness, newdata = testing_small_richness)
  predictions_shannon <- predict(rf_model_shannon, newdata = testing_small_shannon)
  
  # Combining predictions and observed values into data frames
  pred_obs_frame_richness <- data.frame(pred = predictions_richness, obs = testing_river$Richness)
  pred_obs_frame_shannon <- data.frame(pred = predictions_shannon, obs = testing_river$Shannon)
  
  # Save the data frames to individual files
  richness_file_name <- paste0("../R_data/pred_obs_", river_names[i], "_richness.RData")
  shannon_file_name <- paste0("../R_data/pred_obs_", river_names[i], "_shannon.RData")
  save(pred_obs_frame_richness, file = richness_file_name)
  save(pred_obs_frame_shannon, file = shannon_file_name)
}
```

------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Variable importance and response curves

- Variable importance and response curves based on the 5 most important predictors of the richness and shannon index models

```{r}

#----------------------------------------------------------------------------------------------------#
# VARIABLE IMPORTANCE
#----------------------------------------------------------------------------------------------------#

# Extracting variable importance for richness model
importance_values_global <- importance(rf_global)
var_importance_df_global <- data.frame(
  Variable = rownames(importance_values_global),
  Importance = importance_values_global[, 1],
  Model = "Richness Global"
)

# Extracting variable importance for shannon model
importance_values_global_shan <- importance(rf_global_shan)
var_importance_df_global_shan <- data.frame(
  Variable = rownames(importance_values_global_shan),
  Importance = importance_values_global_shan[, 1],
  Model = "Shannon Global"
)

# Sorting the data frames by importance in descending order
var_importance_df_global <- var_importance_df_global[order(-var_importance_df_global$Importance), ]
var_importance_df_global_shan <- var_importance_df_global_shan[order(-var_importance_df_global_shan$Importance), ]

# Taking the top 5 variables for each model
top_vars_global <- head(var_importance_df_global, 5)
top_vars_global_shan <- head(var_importance_df_global_shan, 5)

# Combine the top variables from both models into a single data frame
combined_top_vars <- rbind(
  transform(top_vars_global, Model = "Richness Global"),
  transform(top_vars_global_shan, Model = "Shannon Global")
)

# Renaming variables to match them to ones actually described in paper
combined_top_vars <- combined_top_vars %>%
  mutate(Variable = case_when(
    Variable == "ch" ~ "CH",
    Variable == "elevation" ~ "Elev",
    Variable == "hum" ~ "HMI",
    Variable == "rst" ~ "RST",
    Variable == "sd_ch" ~ "CH_std",
    Variable == "sd_ep" ~ "EP_std",
    Variable == "sum" ~ "WaterA",
    TRUE ~ Variable  # Retain any other values as they are
  ))

# Defining custom colours
my_colors <- c("CH" =  "#31688EFF", "Elev" = "#e31a1c", "HMI" = "#9ACD32", 
               "RST" = "#fada50", "CH_std" = "#440154FF", "EP_std" ="#d7e4f1"  , 
               "WaterA" =  "#35B779FF")
# Defining oder
variable_order <- c("WaterA", "EP_std", "CH_std", "Elev", "RST", "HMI", "CH")
combined_top_vars$Variable <- factor(combined_top_vars$Variable, levels = variable_order)

# Plotting with filled bars
ggplot(combined_top_vars, aes(x = Importance, y = Variable, fill = Variable)) +
  geom_col(alpha=0.7, width=0.85) +
  labs(title = "",
       x = "Mean permutation MSE reduction",
       y = "",
       fill = "") +
  scale_fill_manual(values = my_colors, guide = FALSE) +
  facet_wrap(~ Model, scales = "free_y", ncol = 1) +
  theme_minimal() +
  theme(
    axis.text.y = element_text(size = 8),
    plot.margin = unit(c(0, 3, 0, 3), "cm"),
    strip.text = element_blank(),
    axis.title = element_text(size = 13),
    axis.text  = element_text(size = 11),
    axis.text.y.left = element_text(size = 10)
  ) +
  guides(fill = FALSE)

#----------------------------------------------------------------------------------------------------#
# RESPONSE CURVES
#----------------------------------------------------------------------------------------------------#

# Preparing richness data
selected_df <- training_small_rf_global %>% select(all_of(selected_variables_global_richness))
training_frame <- as.data.frame(selected_df)

# Retrieving variable importance values for richness model
top_vars_global <- top_vars_global$Variable
pdp_results_richness <- list()
for (i in 1:length(top_vars_global)) {
  variable_name <- top_vars_global[i]
  pdp <- partialPlot(rf_global, pred.data = training_frame, x.var = paste(variable_name), plot = FALSE)
  pdp_results_richness[[variable_name]] <- data.frame(Variable = rep(paste(variable_name), length(pdp$x)), x = pdp$x, y = pdp$y)
}
combined_pdp_richness <- do.call(rbind, pdp_results_richness)
combined_pdp_richness$plot_type <- "Richness"

# Preparing Shannon data
selected_df <- training_small_rf_global_shan %>% select(all_of(selected_variables_global_shannon))
training_frame <- as.data.frame(selected_df)

# Retrieving variable importance values for shannon model
top_vars_global_shan <- top_vars_global_shan$Variable
pdp_results_shannon <- list()
for (i in 1:length(top_vars_global_shan)) {
  variable_name <- top_vars_global_shan[i]
  pdp <- partialPlot(rf_global_shan, pred.data = training_frame, x.var = paste(variable_name), plot = FALSE)
  pdp_results_shannon[[variable_name]] <- data.frame(Variable = rep(paste(variable_name), length(pdp$x)), x = pdp$x, y = pdp$y)
}
combined_pdp_shannon <- do.call(rbind, pdp_results_shannon)
combined_pdp_shannon$plot_type <- "Shannon Index"

# Combine both PDP results into a single data frame
combined_pdp <- rbind(combined_pdp_richness, combined_pdp_shannon)

# Adjusting the variable names
combined_pdp <- combined_pdp %>%
  mutate(Variable = case_when(
    Variable == "ch" ~ "CH",
    Variable == "elevation" ~ "Elev",
    Variable == "hum" ~ "HMI",
    Variable == "rst" ~ "RST",
    Variable == "sd_ch" ~ "CH_std",
    Variable == "sd_ep" ~ "EP_std",
    Variable == "sum" ~ "WaterA",
    TRUE ~ Variable  # Retain any other values as they are
  ))

# Plotting response curves using facet_grid to place one plot above the other
ggplot(combined_pdp, aes(x = x, y = y, color = Variable)) +
  geom_line() +
  labs(title = "",
       x = "Variable value",
       y = "") +
  scale_color_manual(values = my_colors) +  # Use defined colors
  theme_minimal() +
  facet_grid(plot_type ~ ., scales = "free_y") +
  theme(
    plot.margin = unit(c(0, 1, 0, 1), "cm"),
    panel.border = element_rect(color = "black", fill = NA),
    legend.box.background = element_rect(color = "black"),
    axis.title = element_text(size = 13),
    axis.text = element_text(size = 11),
    legend.text = element_text(size = 12),
    legend.title = element_text(size = 12),
    strip.text = element_text(size = 12)
  )

```

-----------------------------------------------------------------------------------------------------------------------------------------------------------------
# Combined predicted vs observed plots for model training and testing

- Combined plots for fitting and testing used in the paper are created here 

```{r}
#----------------------------------------------------------------------------------------------------#
# RICHNESS
#----------------------------------------------------------------------------------------------------#

# Setting limits for x and y axis
limits_richness <- c(0, 170)

# Base plot setup
base_plot <- ggplot() +
  labs(x = "Predicted Richness", y = "Observed Richness") +
  lims(x = limits_richness, y = limits_richness) +
  geom_abline(intercept = 0, slope = 1, linetype = "dashed", color = "black") +
  theme_minimal() +
  theme(plot.margin = unit(c(0.3, 0.3, 0.3, 0.3), "cm"),
        axis.text = element_text(size = 10),
        axis.title.x = element_text(margin = margin(t = 13), size=14),
        axis.title.y = element_text(margin = margin(r = 13), size=14),
        plot.background = element_rect(color = "grey", fill = NA, size = 1))

# Combined plot with annotations for evaluation scores
  base_plot +
  geom_point(data = pred_obs_frame_richness, aes(x = pred, y = obs), color = "#9ACD32", alpha = 0.9) +
  geom_point(data = pred_obs_frame_richness_fitting, aes(x = pred, y = obs), color =  "#31688EFF", alpha = 0.6) +
  annotate("text", x = 149.5, y = 5, label = paste("Testing RMSE =", sprintf("%.2f", overall_RMSE_richness)), size = 4.5, color = "#9ACD32") +
  annotate("text", x = 150.2, y = 15, label = paste("Testing %RMSE =", sprintf("%.2f", overall_percent_RMSE_richness)), size = 4.5, color =  "#9ACD32") +
  annotate("text", x = 143.2, y = 25, label = paste("Testing R² =", sprintf("%.2f", overall_R_squared_richness)), size = 4.5, color =  "#9ACD32") +
  annotate("text", x = 147.5, y = 40, label = paste("Fitting RMSE =", sprintf("%.2f", RMSE_richness_fitting)), size = 4.5, color = "#31688EFF") +
  annotate("text", x = 150, y = 50, label = paste("Fitting %RMSE =", sprintf("%.2f", percent_RMSE_richness_fitting)), size = 4.5, color = "#31688EFF") +
  annotate("text", x = 143, y = 60, label = paste("Fitting R² =", sprintf("%.2f", r_squared_richness_fitting)), size = 4.5, color ="#31688EFF")

  
#----------------------------------------------------------------------------------------------------#
# SHANNON
#----------------------------------------------------------------------------------------------------#

# Set limits for x and y axis for Shannon
limits_shannon <- c(0, 6)

# Base plot setup for Shannon
base_plot_shannon <- ggplot() +
  labs(x = "Predicted Shannon", y = "Observed Shannon") +
  lims(x = limits_shannon, y = limits_shannon) +
  geom_abline(intercept = 0, slope = 1, linetype = "dashed", color = "black") +
  theme_minimal() +
  theme(plot.margin = unit(c(0.3, 0.3, 0.3, 0.3), "cm"),
        axis.text = element_text(size = 10),
        axis.title.x = element_text(margin = margin(t = 13), size=14),
        axis.title.y = element_text(margin = margin(r = 13), size=14),
        plot.background = element_rect(color = "grey", fill = NA, size = 1))

# Combined plot with annotations for evaluation scores for Shannon
  base_plot_shannon +
  geom_point(data = pred_obs_frame_shannon, aes(x = pred, y = obs), color = "#9ACD32", alpha = 0.9) +
  geom_point(data = pred_obs_frame_shannon_fitting, aes(x = pred, y = obs), color =  "#31688EFF", alpha = 0.6) +
  annotate("text", x = 5.25, y = 0.2, label = paste("Testing RMSE =", sprintf("%.2f", overall_RMSE_shannon)), size = 4.5, color = "#9ACD32") +
  annotate("text", x = 5.32, y = 0.55, label = paste("Testing %RMSE =", sprintf("%.2f", overall_percent_RMSE_shannon)), size = 4.5, color = "#9ACD32") +
  annotate("text", x = 5.07, y = 0.9, label = paste("Testing R² =", sprintf("%.2f", overall_R_squared_shannon)), size = 4.5, color = "#9ACD32") +
  annotate("text", x = 5.22, y = 1.4, label = paste("Fitting RMSE =", sprintf("%.2f", RMSE_shannon_fitting)), size = 4.5, color = "#31688EFF") +
  annotate("text", x = 5.3, y = 1.75, label = paste("Fitting %RMSE =", sprintf("%.2f", percent_RMSE_shannon_fitting)), size = 4.5, color = "#31688EFF") +
  annotate("text", x = 5.05, y = 2.1, label = paste("Fitting R² =", sprintf("%.2f", r_squared_shannon_fitting)), size = 4.5, color ="#31688EFF")

```