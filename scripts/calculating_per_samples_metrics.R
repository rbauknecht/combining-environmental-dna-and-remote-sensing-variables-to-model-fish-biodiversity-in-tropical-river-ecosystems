# This script is reading in the output of the SWARM pipeline (after LULU cleaning), 
# calculates the biodiversity metrics (Richness and Shannon Index), and then combines the with the 
# corresponding remote sensing variables to have everything in one combined frame. 

# This frame is then saved and can be used for the subsequent analysis

# ------------------------------------------------------------------------------------------------#

# Part 0: Load necessary libraries

# Specify a vector of package names
packages_to_check <- c("dplyr", "tidyverse", "tidyr")

# Loop through each package
for (package_name in packages_to_check) {
  # Check if the package is installed
  if (!requireNamespace(package_name, quietly = TRUE)) {
    # If not installed, install it
    install.packages(package_name)
    
    # Load the package
    library(package_name, character.only = TRUE)
  } else {
    # If already installed, just load it
    library(package_name, character.only = TRUE)
  }
}

# ------------------------------------------------------------------------------------------------#

# Part 1: Loading all necessary functions

source("scripts/helper_functions.R")

# ------------------------------------------------------------------------------------------------#

# Part 2: Calculating the Metrics Frame for each of the rivers 

# Kinabatangan 
metrics_kinabatangan <- calc_metrics("data/swarm_output_clean/kinabatangan_clean.csv", "data/site_sample_mapping/sample_site_mapping_kinabatangan.csv")
# Okavango 
metrics_okavango <- calc_metrics("data/swarm_output_clean/Namibia_clean.csv", "data/site_sample_mapping/sample_site_mapping_all_namibia.csv")
# Magdalena 
metrics_magdalena <- calc_metrics("data/swarm_output_clean/magdalena_clean.csv", "data/site_sample_mapping/sample_site_mapping_magdalena.csv")
# Casamance 
metrics_casamance <- calc_metrics("data/swarm_output_clean/casamance_clean.csv", "data/site_sample_mapping/sample_site_mapping_casamance.csv")
# Maroni
metrics_maroni <- calc_metrics("data/swarm_output_clean/guiana_clean.csv", "data/site_sample_mapping/sample_site_mapping_maroni.csv")
# Oyapock
metrics_oyapock <- calc_metrics("data/swarm_output_clean/guiana_clean.csv", "data/site_sample_mapping/sample_site_mapping_oyapock.csv")


# ------------------------------------------------------------------------------------------------#

# Part 3: Loading the Remote Sensing variables for every river 

# Kinabatangan 
rs_var_kinabatangan <- combine_rs_data("data/rs_variables/Kinabatangan/zonal_mean_statistics_kinabatangan.csv",
                                       "data/rs_variables/Kinabatangan/zonal_std_statistics_kinabatangan.csv",
                                       "data/rs_variables/Kinabatangan/zonal_sum_statistics_kinabatangan.csv",
                                       "data/rs_variables/Kinabatangan/variable_site_mapping_kinabatangan.csv")
# Okavango 
rs_var_okavango <- combine_rs_data("data/rs_variables/all_Namibia/zonal_mean_statistics_all_namibia.csv",
                                   "data/rs_variables/all_Namibia/zonal_std_statistics_all_namibia.csv",
                                   "data/rs_variables/all_Namibia/zonal_sum_statistics_all_namibia.csv",
                                   "data/rs_variables/all_Namibia/variable_site_mapping_Namibia.csv")
# Magdalena 
rs_var_magdalena <- combine_rs_data("data/rs_variables/Magdalena/zonal_mean_statistics_magdalena.csv",
                                    "data/rs_variables/Magdalena/zonal_std_statistics_magdalena.csv",
                                    "data/rs_variables/Magdalena/zonal_sum_statistics_magdalena.csv",
                                    "data/rs_variables/Magdalena/variable_site_mapping_magdalena.csv")
# Casamance 
rs_var_casamance <- combine_rs_data("data/rs_variables/Casamance/zonal_mean_statistics_casamance.csv",
                                    "data/rs_variables/Casamance/zonal_std_statistics_casamance.csv",
                                    "data/rs_variables/Casamance/zonal_sum_statistics_casamance.csv",
                                    "data/rs_variables/Casamance/variable_site_mapping_casamance.csv")
# Guiana_17b == Maroni
rs_var_maroni <- combine_rs_data("data/rs_variables/Guiana_17b/zonal_mean_statistics_guiana_2017_2.csv",
                                 "data/rs_variables/Guiana_17b/zonal_std_statistics_guiana_2017_2.csv",
                                 "data/rs_variables/Guiana_17b/zonal_sum_statistics_guiana_2017_2.csv",
                                 "data/rs_variables/Guiana_17b/variable_site_mapping_guiana_2017_2.csv")
# Guiana_18 == Oyapock
rs_var_oyapock <- combine_rs_data("data/rs_variables/Guiana_18/zonal_mean_statistics_guiana_2018.csv",
                                 "data/rs_variables/Guiana_18/zonal_std_statistics_guiana_2018.csv",
                                 "data/rs_variables/Guiana_18/zonal_sum_statistics_guiana_2018.csv",
                                 "data/rs_variables/Guiana_18/variable_site_mapping_guiana_18.csv")


# ------------------------------------------------------------------------------------------------#

# Part 4: Combining the Biodiversity and Remote Sensing Data

# Kinabatangan
data_kinabatangan <- metrics_kinabatangan %>% left_join(rs_var_kinabatangan, by = c("site" = "site"))
# Casamance
data_casamance <- metrics_casamance %>% left_join(rs_var_casamance, by = c("site" = "site"))
# Magdalena
data_magdalena <- metrics_magdalena %>% left_join(rs_var_magdalena, by = c("site" = "site"))
# Okavango
data_okavango <- metrics_okavango %>% left_join(rs_var_okavango, by = c("site" = "site"))
# Maroni
data_maroni <- metrics_maroni %>% left_join(rs_var_maroni, by = c("site" = "site"))
# Oyapock
data_oyapock <- metrics_oyapock %>% left_join(rs_var_oyapock, by = c("site" = "site"))



# ------------------------------------------------------------------------------------------------#

# Part 5: Making some refinements and combining all rivers into one big frame 

# First of all column with river name should be added to every frame to make them distinguishable 
data_kinabatangan$river <- rep("kinabatangan", nrow(data_kinabatangan))
data_casamance$river <- rep("casamance", nrow(data_casamance))
data_magdalena$river <- rep("magdalena", nrow(data_magdalena))
data_okavango$river <- rep("okavango", nrow(data_okavango))
data_maroni$river <- rep("maroni", nrow(data_maroni))
data_oyapock$river <- rep("oyapock", nrow(data_oyapock))

# Removing points in Maroni data that are not on Maroni river (was visually inspected)
data_maroni <- data_maroni[-c(1:47,54),]

# Removing two samples / one location of Namibia set as this was in lake next to city
data_okavango <- data_okavango[-c(23,24),]

# Selecting the columns that should be standardized to match their ranges
selected_columns <- c("EVI", "ch", "elevation", "ep", "gpp", "hum", "ndvi", "rst", "sd", "slope", "tsi", 
                      "sd_EVI", "sd_ch", "sd_elevation", "sd_ep", "sd_gpp", "sd_hum", "sd_ndvi", "sd_rst", 
                      "sd_sd", "sd_slope", "sd_tsi", "sum")

# Now what will be done is that the data will be normalized twice 
# Once the normalization will be performed on the combined frame, for the global model
# BUT !!! for the local models the normalization will be done river specific since then the other values are not relevant

# STANDARDISATION ON COMBINED DATA
# Combining data
combined_data <- rbind(data_kinabatangan, data_maroni)
combined_data <- rbind(combined_data, data_oyapock)
combined_data <- rbind(combined_data, data_magdalena)
combined_data <- rbind(combined_data, data_casamance)
combined_data <- rbind(combined_data, data_okavango)
# Removing NA values 
combined_data <- na.omit(combined_data)
# Normalizing variables for combined data 
combined_data[selected_columns] <- lapply(combined_data[selected_columns], normalize)

# STANDARDISATION ON INDIVIDUAL RIVERS FOR LOCAL MODELS
# Removing NA values
data_kinabatangan <- na.omit(data_kinabatangan)
data_maroni <- na.omit(data_maroni)
data_oyapock <- na.omit(data_oyapock)
data_magdalena <- na.omit(data_magdalena)
data_casamance <- na.omit(data_casamance)
data_okavango <- na.omit(data_okavango)
# Standardizing 
data_kinabatangan[selected_columns] <- lapply(data_kinabatangan[selected_columns], normalize)
data_maroni[selected_columns] <- lapply(data_maroni[selected_columns], normalize)
data_oyapock[selected_columns] <- lapply(data_oyapock[selected_columns], normalize)
data_magdalena[selected_columns] <- lapply(data_magdalena[selected_columns], normalize)
data_casamance[selected_columns] <- lapply(data_casamance[selected_columns], normalize)
data_okavango[selected_columns] <- lapply(data_okavango[selected_columns], normalize)
# Combining (this frame then contains individually standardized rivers)
combined_data_ind <- rbind(data_kinabatangan, data_maroni)
combined_data_ind <- rbind(combined_data_ind, data_oyapock)
combined_data_ind <- rbind(combined_data_ind, data_magdalena)
combined_data_ind <- rbind(combined_data_ind, data_casamance)
combined_data_ind <- rbind(combined_data_ind, data_okavango)

# Save combined_data frame (standardized as whole)
save(combined_data, file = "R_data/combined_data.Rdata")
# Save combined_data_ind frame (standardized river individually)
save(combined_data_ind, file = "R_data/combined_data_ind.Rdata")